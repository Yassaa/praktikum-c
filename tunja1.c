/*
   Nama Program : tunja1.c
   Tgl buat     : 10 Oktober 2023
   Deskripsi    : menghitung besarnya jumlah tunjangan
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    system("clear");

    int JumlahAnak = 0;
    float GajiKotor = 0.0, Tunjangan = 0.0, PersenTunjangan = 0.0;

    PersenTunjangan = 0.2;
    printf("Gaji Kotor ? ");scanf("%f", &GajiKotor);
    printf("Jumlah Anak ? "); scanf("%d", &JumlahAnak);
    if (JumlahAnak > 2)
    {
        PersenTunjangan = 0.3;
    }

    Tunjangan = PersenTunjangan * GajiKotor;
    printf("Besar Tunjangan = Rp %10.2f\n", Tunjangan);

    return 0;
}