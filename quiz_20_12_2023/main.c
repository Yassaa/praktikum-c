/*
   Soal :
   - Pecah coding pada main menjadi beberapa fungsi dan procedure yang sudah ditentukan. 
   - Pisahkan fungsi dan procedure ke header file yg terpisah (bagi yang paham saja)
   - Quiz dikerjakan di gitlab.
*/

#include <stdio.h>
#include <stdlib.h>


void jdl_aplikasi()
{


}

void gapok_tunja(char gol, char *status, float *gapok, float *tunja)
{


}

float prosen_potongan(float gapok)
{
    float prosen_pot = 0.0;

    return prosen_pot;
}

float potongan(float gapok, float tunja, float prosen_pot)
{
    float pot = 0.0;

    return pot;
}

float gaji_bersih(float gapok, float tunja, float pot)
{
    float gaber = 0.0;

    return gaber;
}

void input(char *nama, char *gol, char *status)
{


}

void output(float gapok, float tunja, float pot, float gaber)
{


}

int main()
{
    system("clear");

    char nama[50];
    char gol = ' ';
    char status[10];

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    printf("*************************\n");
    printf("*  Aplikasi Hitung Gaji *\n");
    printf("*************************\n\n");

    printf("Nama Karyawan       : ");
    scanf("%s", nama);
    printf("Golongan (A\\B)      : ");
    scanf(" %c", &gol);
    printf("Satus (Nikah\\Belum) : ");
    scanf("%s", status);

    switch (gol)
    {
    case 'A':
        gapok = 200000;
        if (strcmp(status, "Nikah") == 0)
        {
            tunja = 50000;
        }
        else
        {
            if (strcmp(status, "Belum") == 0)
            {
                tunja = 25000;
            }
        }
        break;
    case 'B':
        gapok = 350000;
        if (strcmp(status, "Nikah") == 0)
        {
            tunja = 75000;
        }
        else
        {
            if (strcmp(status, "Belum") == 0)
            {
                tunja = 60000;
            }
        }
        break;
    default:
        printf(" Salah Input Golongan !!!\n");
        return 0;
    }

    if (gapok > 300000)
    {
        prosen_pot = 0.1;
    }

    pot = (gapok + tunja) * prosen_pot;
    gaber = (gapok + tunja) - pot;

    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);

    return 0;
}
