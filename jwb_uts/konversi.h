#include <stdio.h>
#include <stdlib.h>

float Celcius_Fahrenheit(float c)
{
    float hsl = 0.0;
    hsl = (c * 9 / 5) + 32;
    return hsl;
}

float Celcius_Kelvin(float c)
{
    float hsl = 0.0;
    hsl = c + 273.15;
    return hsl;
}

void konversi()
{
    system("clear");

    int awal = 0;
    int akhir = 0;
    int step = 1;
    float c = 0.0;
    float f = 0.0;
    float k = 0.0;

    printf("*****************************\n");
    printf("*       Tabel Konversi      *\n");
    printf("*****************************\n");
    printf("\n");

    printf("Masukan suhu awal  = ");
    scanf("%d", &awal);
    printf("Masukan step       = ");
    scanf("%d", &step);
    printf("Masukan suhu akhir = ");
    scanf("%d", &akhir);
    printf("\n");

    printf("---------------------------------\n");
    printf("| Celcius | Fahrenheit | Kelvin |\n");
    printf("---------------------------------\n");

    for (int i = awal; i < akhir; i += step)
    {
        c = i;
        f = Celcius_Fahrenheit(c);
        k = Celcius_Kelvin(c);
        printf("|%7.2f |%11.2f |%7.2f |\n", c, f, k);
    }

    printf("---------------------------------\n");

    printf("\n");
    printf("Kembali ke menu dalam 5 detik !!!\n");
}
