#include <stdio.h>
#include <stdlib.h>

void kalkulator()
{
    system("clear");

    float bil1 = 0.0;
    float bil2 = 0.0;
    char opt = ' ';
    float hsl = 0.0;

    printf("*****************************\n");
    printf("*    Kalkulator Sederhana   *\n");
    printf("*****************************\n");
    printf("\n");
    printf("Masukkan Bilangan Ke 1     : ");
    scanf("%f", &bil1);
    printf("Masukan Operator (x,:,+,-) : ");
    scanf(" %c", &opt);  // Note the space before %c to consume the newline character.
    printf("Masukkan Bilangan Ke 2     : ");
    scanf("%f", &bil2);

    switch (opt)
    {
    case 'x':
        hsl = bil1 * bil2;
        break;
    case ':':
        hsl = bil1 / bil2;
        break;
    case '+':
        hsl = bil1 + bil2;
        break;
    case '-':
        hsl = bil1 - bil2;
        break;
    }

    printf("Bilangan ke 1 %c Bilangan ke 2 = %.2f %c %.2f = %.2f\n", opt, bil1, opt, bil2, hsl);

    printf("\n");
    printf("Kembali ke menu dalam 5 detik !!!\n");
}
