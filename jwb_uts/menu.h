#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "kalkulator.h"
#include "konversi.h"
#include "bintang.h"

int menu()
{
    int pilih = 0;

    system("clear");

    printf("*****************************\n");
    printf("*        Jawaban UTS        *\n");
    printf("*****************************\n");
    printf("\n");
    printf("*****************************\n");
    printf("*            Menu           *\n");
    printf("*****************************\n");
    printf("* 1. Kalkulator Sederhana   *\n");
    printf("* 2. Tabel Konversi Suhu    *\n");
    printf("* 3. Cetak Bintang          *\n");
    printf("*****************************\n");
    printf("* 0. Keluar                 *\n");
    printf("*****************************\n");
    printf("Pilih Menu : ");
    scanf("%d", &pilih);
    return pilih;
}

void menu_dipilih(int no_menu)
{
    switch (no_menu)
    {
    case 1:
        kalkulator();
        sleep(5);
        break;
    case 2:
        konversi();
        sleep(5);
        break;
    case 3:
        bintang();
        sleep(5);
        break;
    case 0:
        printf("Keluar Program dalam 2 detik !!!\n");
        sleep(2);
        break;
    default:
        printf("Salah Masukan Menu !!!\n");
        sleep(2);
        break;
    }
}
