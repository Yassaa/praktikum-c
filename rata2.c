/*
   Nama Program : rata2.c
   Tgl buat     : 7 November 2023
   Deskripsi    : menghitung rata-rata
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
  system("clear");

    int i=0, N=0;  // counter and number of data
    float Data=0.0, Rata=0.0, Total=0.0;

    printf("Banyaknya data : ");
    scanf("%d", &N);
    Total = 0;

    for (i = 1; i <= N; i++) {
        printf("Data ke %d : ", i);
        scanf("%f", &Data);
        Total += Data;
    }

    Rata = Total / N;
    printf("Banyaknya Data : %d\n", N);
    printf("Total Nilai Data : %.2f\n", Total);
    printf("Rata-rata nilai Data : %.2f\n", Rata);

  return 0;
}