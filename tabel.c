/*
   Nama Program : tabel.c
   Tgl buat     : 7 November 2023
   Deskripsi    : 
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
  system("clear");
  
  int x=0, i=0, pangkat=0, hasilpangkat=0;
    
    printf("x  1/x  x^2   x^3\n");
    printf("-----------------------------------\n");

    x = 1;
    while (x <= 10) {
        printf("%5d %8.5f", x, 1.0 / x);
        pangkat = 2;
        while (pangkat <= 3) {
            hasilpangkat = 1;
            for (i = 1; i <= pangkat; i++) {
                hasilpangkat *= x;
            }
            printf("%8d", hasilpangkat);
            pangkat++;
        }
        printf("\n");
        x++;
    }

  return 0;
}