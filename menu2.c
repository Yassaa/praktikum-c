/*
   Nama Program : menu2.c
   Tgl buat     : 7 November 2023
   Deskripsi    : membuat menu
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

const double pi = 3.14;

int main()
{
  system("clear");

    int pilih=0;
    float sisi=0.0, jari=0.0, tinggi=0.0;

    do {
        printf("<<< Menu >>>\n\n");
        printf("1. Menghitung Isi Kubus\n");
        printf("2. Menghitung Luas Lingkaran\n");
        printf("3. Menghitung Isi Silinder\n");
        printf("0. Keluar Program\n");
        printf("Pilih Nomor : ");
        scanf("%d", &pilih);

        switch (pilih) {
            case 1:
                printf("Panjang Sisi Kubus : ");
                scanf("%f", &sisi);
                printf("Isi Kubus : %.2f\n", sisi * sisi * sisi);
                break;

            case 2:
                printf("Jari-jari lingkaran : ");
                scanf("%f", &jari);
                printf("Luas Lingkaran : %.2f\n", pi * jari * jari);
                break;

            case 3:
                printf("Jari-jari lingkaran : ");
                scanf("%f", &jari);
                printf("Tinggi Silinder : ");
                scanf("%f", &tinggi);
                printf("Isi Silinder : %.2f\n", pi * jari * jari * tinggi);
                break;
        }
        
        if (pilih != 0) {
            getch();
        }
    } while (pilih != 0); 

  return 0;
}

